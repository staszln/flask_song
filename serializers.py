from marshmallow import Schema, fields, validate


class SongSchema(Schema):
    song_name = fields.Str(required=True, validate=validate.Length(min=2))
    song_year = fields.Int(required=False, validate=validate.Range(min=1900, max=2050))
    song_text = fields.Str(required=False)
    origin_lang = fields.Str(required=False, validate=validate.OneOf(['uk', 'en', 'fr', 'de']))


class AlbumSchema(Schema):
    album_name = fields.Str(required=True, validate=validate.Length(min=2))
    album_year = fields.Int(required=False, validate=validate.Range(min=1900, max=2050))
    album_info = fields.Str(required=False)


class ArtistSchema(Schema):
    artist_name = fields.Str(required=True)
    artist_info = fields.Str(required=False)
