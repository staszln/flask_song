import json
import models
import serializers
from models import artist as artist_model, song as song_model, album as album_model, artist_table as artist_table_model
from models import delete_smth, commit_me, create_smth

# def db_request(query):
#     data_base = os.environ['db_name']
#     con = sqlite3.connect(data_base)
#     con.row_factory = sqlite3.Row
#     cur = con.cursor()
#     cur.execute(query)
#     con.commit()
#     result = [dict(i) for i in cur.fetchall()]
#     con.close()
#     return result


def search(search_string):
    result_song = song_model.query.filter(song_model.song_name.like(f'%{search_string}%')).all()
    # [dict(item) for item in result_song]
    result_album = album_model.query.filter(album_model.album_name.like(f'%{search_string}%')).all()
    # [dict(item) for item in result_album]
    result_artist = artist_model.query.filter(artist_model.artist_name.like(f'%{search_string}%')).all()
    # [dict(item) for item in result_artist]
    res_song = [dict(item) for item in result_song[1:]]
    res_album = [dict(item) for item in result_album[1:]]
    res_artist = [dict(item) for item in result_artist[1:]]
    # result = {
    #     'artist': res_song,
    #     'album': res_album,
    #     'song': res_artist
    # }
    # search_dict = {}
    # for table in ['artist', 'album', 'song']:
    #     search_dict.update(
    #         f'{table}'_model.
    #     )
    #     search_dict.update({table: db_request(
    #         f'''SELECT * FROM {table} WHERE {table}_name like "%{search_string}%"'''
    #     )})
    return {'artist_list': res_song, 'song_list': res_album,
            'album_list': res_artist}


def artist(artist):
    result = album_model.query\
        .join(artist_table_model, artist_table_model.album_id==album_model.album_id)\
        .join(artist_model, artist_table_model.artist_id==artist_model.artist_id)\
        .add_columns(artist_model.artist_name, artist_model.artist_info,\
        album_model.album_name, album_model.album_id).\
        filter(artist_model.artist_name == artist).all()

    # query = f""" SELECT DISTINCT
    #                        artist.artist_name,
    #                        artist.artist_info,
    #                        album.album_name,
    #                        album.album_id
    #                     FROM artist_table JOIN artist on
    #                         artist_table.artist_id = artist.artist_id
    #                     JOIN album on artist_table.album_id = album.album_id
    #                     WHERE artist.artist_name = "{artist}"
    #                     """
    # result = db_request(query)
    # res_list = []
    # for itm in result:
    #     res_list.append(dict(itm))
    # return res_list

    # list_result = [
    #     {
    #         'artist_name': res.artist_name,
    #         'artist_info': res.artist_info,
    #         'album_name': res.album_name,
    #         'album_info': res.album_info
    #     }
    #     for res in result
    # ]
    # return list_result
    info = [dict(item) for item in result]
    return {'album_list': info, 'artist_name': artist, 'artist_info': info[0]['artist_info']}


def album(artist_name, album_name):
    result = song_model.query\
    .join(artist_table_model, song_model.song_id==artist_table_model.song_id)\
    .join(album_model, artist_table_model.album_id==album_model.album_id)\
    .add_columns(artist_table_model.song_id, artist_table_model.track_num,
                 song_model.song_name, song_model.song_year,
                 album_model.album_name, album_model.album_info)\
    .filter(album_model.album_name == album_name, artist_model.artist_name == artist_name).all()
    # query = f"""SELECT
    #                 artist_table.song_id,
    #                 artist_table.track_num,
    #                 song.song_name,
    #                 song.song_year,
    #                 album.album_name,
    #                 album.album_info
    #             FROM song JOIN artist_table on
    #                 song.song_id = artist_table.song_id
    #             JOIN album on
    #                 artist_table.album_id = album.album_id
    #             JOIN artist on
    #                 artist.id_artist = artist_table.artist_id
    #             WHERE album.album_name = "{album}" and artist.artist_name = "{artist}"
    #         """
    # result = db_request(query)
    info = [dict(item) for item in result]
    return {'song_list': info, 'artist_name': artist_name,
            'album_name': album_name, 'album_info': info[0]['album_info']}


def song(artist, song):
    result = song_model.query\
    .join(artist_table_model, song_model.song_id==artist_table_model.song_id)\
    .join(artist_model, artist_model.artist_id==artist_table_model.artist_id)\
    .add_columns(artist_model.artist_name, song_model.song_id, song_model.song_name,
                 song_model.song_year, song_model.song_text)\
    .filter(song_model.song_name == song, artist_model.artist_name == artist).all()
    # query = f"""SELECT
    #                song.song_id,
    #                artist.artist_name,
    #                song.song_name,
    #                song.song_year,
    #                song_text
    #             FROM song JOIN artist_table on
    #                 song.song_id = artist_table.song_id
    #             JOIN artist on
    #                 artist.id_artist = artist_table.artist_id
    #             WHERE song.song_name = "{song}" and
    #                 artist.artist_name = "{artist}"
    #             """
    # result = db_request(query)
    info = [dict(item) for item in result]
    return {'song_name': song, 'artist_name': artist, 'song_text': info[0]['song_text']}


def smart_insert_album(album_name, album_year, album_info):
    album_search = album_model.query.filter(album_model.album_name.like(f'%{album_name}%')).first()
    # query = f"""SELECT DISTINCT album.album_id
    #             FROM album
    #             WHERE album.album_name = "{album_name}"
    #                         """
    # album_data = db_request(query)
    if album_search is None:
        new_album = models.album()
        new_album.album_name = album_name
        new_album.album_year = album_year
        new_album.album_info = album_info
        models.create_smth(new_album)
        album_search = album_model.query.filter(album_model.album_name.like(f'%{album_name}%')).\
            filter(album_model.album_year.like(f'%{album_year}%')).\
            filter(album_model.album_info.like(f'%{album_info}%')).first()
        # query_insert_artist = f"""INSERT INTO album
        # (album_name, album_year, album_info)
        # VALUES ("{album_name}", "{album_year}", "{album_info}")
        # """
        # db_request(query_insert_artist)
        # album_data = db_request(query)
    return album_search.album_id


def smart_insert_artist(artist_name, artist_info):
    artist_search = artist_model.query.filter(artist_model.artist_name.like(f'%{artist_name}%')).first()
    # query = f"""SELECT DISTINCT album.album_id
    #             FROM album
    #             WHERE album.album_name = "{album_name}"
    #                         """
    # album_data = db_request(query)
    if artist_search is None:
        new_artist = models.artist()
        new_artist.artist_name = artist_name
        new_artist.artist_info = artist_info
        create_smth(new_artist)
        artist_search = artist_model.query.filter(artist_model.artist_name==artist_name).\
            filter(artist_model.artist_info.like(f'%{artist_info}%')).first()
        # query_insert_artist = f"""INSERT INTO album
        # (album_name, album_year, album_info)
        # VALUES ("{album_name}", "{album_year}", "{album_info}")
        # """
        # db_request(query_insert_artist)
        # album_data = db_request(query)
    #
    # query = f"""SELECT artist.artist_id
    #                         FROM artist
    #                         WHERE artist.artist_name = "{artist_name}"
    #                         """
    # artist_data = db_request(query)
    # if not artist_data:
    #     query_insert_artist = f"""INSERT INTO artist
    #     (artist_name, artist_info)
    #     VALUES ("{artist_name}", "{artist_info}")
    #     """
    #     db_request(query_insert_artist)
    #     artist_data = db_request(query)
    return artist_search.album_id


def insert_song(data_song_info):
    new_song = models.song()
    new_song.song_name = data_song_info['song_name']
    new_song.song_year = data_song_info['year']
    new_song.song_text = data_song_info['song_text']
    new_song.origin_lang = data_song_info['lang']
    models.create_smth(new_song)
    # query = f"""
    # INSERT INTO song
    # (song_name, song_text, song_year, origin_lang)
    # VALUES("{data_song_info['song_name']}",
    #         "{data_song_info['song_text']}",
    #         "{data_song_info['year']}",
    #         "{data_song_info['lang']}")
    # """
    # db_request(query)


def get_song_id(data_song_info):
    song_obj = song_model.query\
    .filter(song_model.song_name==data_song_info['song_name'])\
    .filter(song_model.song_text==data_song_info['song_text'])\
    .filter(song_model.song_year==data_song_info['year'])\
    .filter(song_model.origin_lang==data_song_info['lang']).first()
    # query = f"""SELECT song.song_id FROM song
    #             WHERE
    #             song.song_name = "{data_song_info['song_name']}" AND
    #             song.song_text = "{data_song_info['song_text']}" AND
    #             song.song_year = "{data_song_info['year']}" AND
    #             song.origin_lang = "{data_song_info['lang']}"
    #     """
    # song_id = db_request(query)[0]['song_id']
    song_id = song_obj.song_id
    return song_id


def add_to_tracklist(song_id, artist_id, album_id, track_num):
    new_track = models.artist_table()
    new_track.track_num = track_num
    new_track.song_id = song_id
    new_track.album_id = album_id
    new_track.artist_id = artist_id
    models.create_smth(new_track)


def add_song(song_data):
    errors = serializers.AlbumSchema.validate(song_data['song_info'])
    if errors:
        return errors
    song_obj = serializers.SongSchema().load(song_data['song_info'])
    insert_song(song_obj)
    song_id = get_song_id(song_obj)
    if song_data['artist_info'][0]['artist_name'] == '':
        add_to_tracklist(song_id, '0', '0', '0')
    else:
        for artist_data in song_data['artist_info']:
            if artist_data['artist_name'] != "":
                artist_id = smart_insert_artist(artist_data['artist_name'], artist_data['artist_info'])
                for album_data in artist_data['album_info']:
                    if album_data['album_name'] != "" and album_data['year'] != "":
                        album_id = smart_insert_album(album_data['album_name'],
                                                      album_data['year'], album_data['info'])
                        add_to_tracklist(song_id, artist_id, album_id, album_data['track_num'])


def song_update(song_data):
    errors = serializers.AlbumSchema.validate(song_data['song_info'])
    if errors:
        return errors
    data_song_info = song_data['song_info']
    song_id = get_song_id(data_song_info)
    song_obj = song_model.query.get(song_id).first()
    old_song_data = song(song_data['song_info']['song_name'],
                         song_data['artist_info']['artist_name'])
    if data_song_info['song_text'] != old_song_data['song_text']:
        song_obj.song_text = data_song_info['song_text']
        models.commit_me()
        # query = f'''UPDATE song
        #             SET song_text = "{data_song_info['song_text']}"
        #             WHERE song.song_id = "{old_song_data['song_id']}"'''
        # db_request(query)
    if data_song_info['year'] != old_song_data['song_year']:
        song_obj.song_year = data_song_info['year']
        models.commit_me()
        # query = f'''UPDATE song
        #             SET song_year = "{data_song_info['year']}"
        #             WHERE song.song_id = "{old_song_data['song_id']}"'''
        # db_request(query)
    if data_song_info['lang'] != old_song_data['origin_lang']:
        song_obj.origin_lang = data_song_info['lang']
        # query = f'''UPDATE song
        #                     SET origin_lang = "{data_song_info['lang']}"
        #                     WHERE song.song_id = "{old_song_data['song_id']}"'''
        # db_request(query)


def song_delete(song_data):
    errors = serializers.AlbumSchema.validate(song_data['song_info'])
    if errors:
        return errors
    data_song_info = song_data['song_info']
    song_id = get_song_id(data_song_info)
    song_obj = song_model.query.get(song_id).first()
    delete_smth(song_obj)
    # query = f'''
    #     DELETE FROM artist_table WHERE artist_table.song_id = "{song_id}"
    # '''
    # db_request(query)
    # query = f'''
    #     DELETE FROM artist_table WHERE artist_table.song_id = "{song_id}"
    # '''
    # db_request(query)


def smart_insert_album(album_name, album_year, album_info):
    album_search = album_model.query.filter(album_model.album_name==album_name).first()
    # query = f"""SELECT DISTINCT album.album_id
    #             FROM album
    #             WHERE album.album_name = "{album_name}"
    #                         """
    # album_data = db_request(query)
    if album_search in None:
        new_album = models.album()
        new_album.album_name = album_name
        new_album.album_year = album_year
        new_album.album_info = album_info
        create_smth(new_album)
        album_search = album_model.query.filter(album_model.album_name == album_name).first()
        # query_insert_artist = f"""INSERT INTO album
        # (album_name, album_year, album_info)
        # VALUES ("{album_name}", "{album_year}", "{album_info}")
        # """
        # db_request(query_insert_artist)
        # album_data = db_request(query)
    return album_search.album_id


# def smart_insert_artist(artist_name, artist_info):
#
#     query = f"""SELECT artist.artist_id
#                             FROM artist
#                             WHERE artist.artist_name = "{artist_name}"
#                             """
#     artist_data = db_request(query)
#     if not artist_data:
#         query_insert_artist = f"""INSERT INTO artist
#         (artist_name, artist_info)
#         VALUES ("{artist_name}", "{artist_info}")
#         """
#         db_request(query_insert_artist)
#         artist_data = db_request(query)
#     return artist_data[0]['artist_id']


def insert_song(data_song_info):
    new_song = models.song()
    new_song.song_name = data_song_info['song_name']
    new_song.song_text = data_song_info['song_text']
    new_song.song_year = data_song_info['year']
    new_song.origin_lang = data_song_info['lang']
    models.create_smth(new_song)
    # query = f"""
    # INSERT INTO song
    # (song_name, song_text, song_year, origin_lang)
    # VALUES("{data_song_info['song_name']}",
    #         "{data_song_info['song_text']}",
    #         "{data_song_info['year']}",
    #         "{data_song_info['lang']}")
    # """
    # db_request(query)


# def get_song_id(data_song_info):
#     query = f"""SELECT song.song_id FROM song
#                 WHERE
#                 song.song_name = "{data_song_info['song_name']}" AND
#                 song.song_text = "{data_song_info['song_text']}" AND
#                 song.song_year = "{data_song_info['year']}" AND
#                 song.origin_lang = "{data_song_info['lang']}"
#         """
#     song_id = db_request(query)[0]['song_id']
#     return song_id


def add_to_tracklist(song_id, artist_id, album_id, track_num):
    new_tracklist = models.artist_table()
    new_tracklist.song_id = song_id
    new_tracklist.artist_id = artist_id
    new_tracklist.album_id = album_id
    new_tracklist.track_num = track_num
    create_smth(new_tracklist)
    # query = f"""
    # INSERT INTO artist_table (artist_id, song_id, album_id, track_num)
    # VALUES ("{artist_id}", "{song_id}", "{album_id}", "{track_num}")
    # """
    # db_request(query)


def add_song(song_data):
    data_song_info = song_data['song_info']
    insert_song(data_song_info)
    song_id = get_song_id(data_song_info)
    if song_data['artist_info'][0]['artist_name'] == '':
        add_to_tracklist(song_id, '0', '0', '0')
    else:
        for artist_data in song_data['artist_info']:
            if artist_data['artist_name'] != "":
                artist_id = smart_insert_artist(artist_data['artist_name'], artist_data['artist_info'])
                for album_data in artist_data['album_info']:
                    if album_data['album_name'] != "" and album_data['year'] != "":
                        album_id = smart_insert_album(album_data['album_name'],
                                                      album_data['year'], album_data['info'])
                        add_to_tracklist(song_id, artist_id, album_id, album_data['track_num'])


def song_update(song_data):
    data_song_info = song_data['song_info']
    song_id = get_song_id(data_song_info)
    song_obj = song_model.query.get(song_id)
    old_song_data = song(song_data['song_info']['song_name'],
                         song_data['artist_info']['artist_name'])
    if data_song_info['song_text'] != old_song_data['song_text']:
        song_obj.song_text = data_song_info['song_text']
        # query = f'''UPDATE song
        #             SET song_text = "{data_song_info['song_text']}"
        #             WHERE song.song_id = "{old_song_data['song_id']}"'''
        # db_request(query)
    if data_song_info['year'] != old_song_data['song_year']:
        song_obj.song_year = data_song_info['year']
        # query = f'''UPDATE song
        #             SET song_year = "{data_song_info['year']}"
        #             WHERE song.song_id = "{old_song_data['song_id']}"'''
        # db_request(query)
    if data_song_info['lang'] != old_song_data['origin_lang']:
        song_obj.origin_lang = data_song_info['lang']
        # query = f'''UPDATE song
        #                     SET origin_lang = "{data_song_info['lang']}"
        #                     WHERE song.song_id = "{old_song_data['song_id']}"'''
        # db_request(query)
    commit_me()


def song_delete(song_data):
    data_song_info = song_data['song_info']
    song_id = get_song_id(data_song_info)
    song_obj = song_model.query.get(song_id)
    delete_smth(song_obj)
    # query = f'''
    #     DELETE FROM artist_table WHERE artist_table.song_id = "{song_id}"
    # '''
    # db_request(query)
    # query = f'''
    #     DELETE FROM artist_table WHERE artist_table.song_id = "{song_id}"
    # '''
    # db_request(query)
