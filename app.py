from flask import Flask, request, render_template
import json
import user_data
from models import db
import os


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{os.environ.get("db_name")}'
db.init_app(app)


@app.route('/search')
def search():
    request_args = request.args
    search_result = user_data.search(request_args['search_string'])
    return render_template('search.html', **search_result)


@app.route('/artist/<artist_name>/')
def artist(artist_name):
    artist_data = user_data.artist(artist_name)
    return render_template('artist.html', **artist_data)


@app.route('/artist/<artist_name>/album/<album_name>/')
def album(artist_name, album_name):
    album_data = user_data.album(artist_name, album_name)
    return render_template('album.html', **album_data)


@app.route('/artist/<artist_name>/song/<song_name>/', methods=['GET', 'PUT', 'DELETE'])
def song(artist_name, song_name):
    if request.method == 'PUT':
        user_data.song_update(request.json)
    elif request.method == 'DELETE':
        user_data.song_delete(request.json)
    elif request.method == 'GET':
        song_data = user_data.song(artist_name, song_name)
        return render_template('song.html', **song_data)


@app.route('/add/', methods=['GET', 'POST'])
def add_song():
    if request.method == 'POST':
        user_data.add_song(request.json)
        return 'cool'
    else:
        return {}


if __name__ == '__main__':
    app.run()
